

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Ignore;
import org.junit.Test;

class JavaSetupTest {
    @Test
    @Ignore // Spotbugs setup error in test
    void testConfigure() {
        Project projectStub = ProjectBuilder.builder()
                .build()
        projectStub.buildscript {
            repositories {
                mavenCentral()
            }
            dependencies {
                // these are globally visible, but plugins still need apply plugin ... to be enabled
                classpath("gradle.plugin.com.github.spotbugs:spotbugs-gradle-plugin:${ToolVersions.spotbugsPlugin}")
                classpath("net.ltgt.gradle:gradle-errorprone-plugin:${ToolVersions.errorPronePlugin}")
                classpath("info.solidsoft.gradle.pitest:gradle-pitest-plugin:${ToolVersions.pitestPlugin}")
            }
        }
    }
}
