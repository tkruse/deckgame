/**
 * Versions for build tool dependencies
 */
interface ToolVersions {
    static jdk = '1.17'
    static checkstyle = '10.9.3'
    static pmd = '6.55.0'
    static spotbugsPlugin = '5.0.14'
    static spotbugs = '4.7.3'
    static errorProne = '2.18.0'
    static errorPronePlugin = '3.0.1'
    static jacoco = '0.8.9'
}

interface Versions {

    static jackson = '2.13.1'
    static guava = '31.1-jre'

    static mapstruct = '1.4.2.Final'
    static jsr305 = '3.0.2'

    static logback = '1.4.6'
    static slf4j = '2.0.7'

    static mockito2 = '5.3.0'
    static powermock = '2.0.9'
    static hamcrest = '1.3'
    static restassured = '4.4.0' // also for io.rest-assured:json-path
    static junit5 = '5.8.1'
    static immutables = '2.9.2'
    static lombok = '1.18.26'
}


interface Libraries {

    static jackson_core = "com.fasterxml.jackson.core:jackson-core:${Versions.jackson}"
    static jackson_databind = "com.fasterxml.jackson.core:jackson-databind:${Versions.jackson}"
    static jackson_annotations = "com.fasterxml.jackson.core:jackson-annotations:${Versions.jackson}"

    static commons_lang3 = "org.apache.commons:commons-lang3:3.12.0"
    static commons_coll4 = "org.apache.commons:commons-collections4:4.4"
    static commons_io = "commons-io:commons-io:2.11.0"
    static commons_text = 'org.apache.commons:commons-text:1.10.0'

    static guava = "com.google.guava:guava:${Versions.guava}"

    static jsr305 = "com.google.code.findbugs:jsr305:${Versions.jsr305}"
    static spotbugsAnnotations = "com.github.spotbugs:spotbugs-annotations:${ToolVersions.spotbugs}"

    static immutables_value = "org.immutables:value:${Versions.immutables}"
    static immutables_value_annotations = "org.immutables:value:${Versions.immutables}:annotations"
    static immutables_builder = "org.immutables:builder:${Versions.immutables}"
    static lombok = "org.projectlombok:lombok:${Versions.lombok}"

    static slf4j_api = "org.slf4j:slf4j-api:${Versions.slf4j}"
    static jcl_over_slf4j = "org.slf4j:jcl-over-slf4j:${Versions.slf4j}"
    static log4j_over_slf4j = "org.slf4j:log4j-over-slf4j:${Versions.slf4j}"
    static logback = "ch.qos.logback:logback-classic:${Versions.logback}"

    static mapstruct = "org.mapstruct:mapstruct-jdk8:${Versions.mapstruct}"
    static mapstruct_processor = "org.mapstruct:mapstruct-processor:${Versions.mapstruct}"


    // test dependencies
    static assertj_core = "org.assertj:assertj-core:3.24.2"
    static mockito = "org.mockito:mockito-core:${Versions.mockito2}"
    static hamcrest = "org.hamcrest:hamcrest-all:${Versions.hamcrest}"
    static powermock_mockito2 = "org.powermock:powermock-api-mockito2:${Versions.powermock}"
    static awaitility = "org.awaitility:awaitility:4.1.1"
    static wiremock = "com.github.tomakehurst:wiremock:2.15.0"
    static rest_assured = "io.rest-assured:rest-assured:${Versions.restassured}"
}
