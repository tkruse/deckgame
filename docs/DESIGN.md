# Deckgame

Project to validate a concept of an abstract adventure game.
In the game, a party of (1 or more) adventurers sets out multiple times on quests.
In each quest the party explores and follows a story-path based on a deck.
A deck contains cards representing events that occur during the adventure.
Each event may provide players with a choice of resolution depending on prerequisites and how much resources players are
willing to invest.
An event is a situation that basically modifies the party resources, based on player decisions and random.
Any event might give the party loot, which can be kept beyond the quest if the quest is successful.
As the quest progresses, a path is built up as the sequence of events the party lives through.
A party has limited resources available which can change as an effect of events.

To be successful, the party needs to reach a finishing state without running out of any resource needed to continue on.
A finishing state could be returning to the beginning, or reaching an event card.

Before each quest, the players chose the deck of cards from which to draw next locations/events.
By choosing this deck, players decide on the difficulty and nature of the quest.

This principle allows a variety of fantasy scenarios to be played using the same mechanics and few game materials.

As a multiplayer game, this can be a cooperative game with some adversarial elements.

## Deck Design

One quest can consist of multiple phases, represented by multiple draw decks.
By playing with one deck, resources of the party change.
Players need to make decisions to manage resources.

Quests can also be parts of a larger quests, a quest reward may be a MacGuffin to unlock the next quest.

Mathematically, a quest is a transformation from the initial party resources to the resources at the end of a successful
quest.

### Balancing Decks

A quest should neither be impossible nor too easy.
Players can get rewards during the quest continuously, and decide when to end this quest.
As each card transforms resources with a certain probability, the expected impact of a card on exhaustion can be
measured.
Composing a deck of cards thus needs to balance card resource consumption against initial resources.
A "goal" card can thus define which card types are required to build a "fair" path to this goal.
Alternatively, a "goal" card can define minimum conditions to be played, ensuring the quest is not too easy, but then
the quest may become impossible.

"Side quests" can be used to provide players with a way to spend more resources for more reward at the risk of earlier
exhaustion.
Each card can give players options to gamble more resources for more rewards.

If players want to customize their decks, they might need guidance on how to achieve balancing.

## Collection meta-game

By playing quests successfully, players gain new cards which can make future quests easier, more challenging/rewarding,
longer, ...
This represents in an abstract way the party development gaining insight into the given domain.
The collection of cards defines the space of possible quests.
Players are free to create any quest by combining cards into quest decks.

## Motivators

+ Curiosity
+ Immersion
+ Collecting more cards
+ Character growth
    + can win harder fights
    + has more event options
    + can draw/place better cards
+ Random rewards
+ Creativity (when building decks)

# Inspirations

* Frostpunk story ark
* Loop hero path and balancing decided by players under some eternal pressure
* Betrayal at baldurs gate characters moving on evolving cards map

# Other Ideas

* Hand
    * decide when to play what card to achieve personal goals
* challenges
    * single dice throw
    * multi-round battles
* Big rewards
    * when winning an event
    * when reaching quest goal
    * when finishing quest
    * when achieving mission card
* Cards can be combined on the path
    * make cards more challenging and more rewarding
    * spend more effort to improve outcomes
    * add a modification to reduce a cards challenge
* Cards can evolve
    * evolution changes any attribute
    * based on usage in path
    * based on selected events
    * based on event result
    * merging cards
    * player chosen advancement
* Card evolution
    * forest, dark forest, dark evil forest, werewolf forest
    * plains, path, road, crossing
    * grass, camp, hut, house, mansion
    * hill, cave, tomb
* Suspense
    * Times grow darker
* Distance to goal
    * fixed deck size until goal
    * fixed resource counter on cards on path
    * shuffling strategy
    * deck max size
    * number decreased randomly according to cards
* Questing rewards
    * player-designed cards
* Player evolution
    * Generic players (peasants) can accumulate cards to become warriors, tanks, healers, thieves, mages, glass cannons