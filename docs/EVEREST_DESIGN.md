# Everest expedition design idea

This idea attempts to create a scenario similar to May 10th 1996, when a commercial expedition to Everest ended in disaster with many people dead.

Core concepts:
* The path is camp4 to summit, with ice falls, slopes, steps, balconies, ...
* Characters in the game are guides, sherpas and members
* Characters have health, oxygen deprivation resistance, temperature, energy
* Each character has different initial properties (climbing skill, oxygen reserves, carry strength)
* Each character can be equipped with certain gear (ladders, ropes, oxygen, pickaxe, medecine, ...)
* Each character belongs to one company
* Characters may move up different paths, but there can be bottlenecks with single path
* Each player controls characters of one company, turn based, one at a time
* Per turn, a character can move, use equipment, interact with other character
* Characters can become distressed, uncontrollable
* Distressed characters can be controlled again if rescued, controlled by controller of rescuer, or regain control themselves
* Positive points are awarded for summiting surviving characters (randomized reward 1-3 points, weaker members give more points)
* Positive points for guiding distressed players into camp
* Negative points are awarded for dying characters of any party (-20 points per character)
* The expedition starts with a "window" of good weather, and bad weather approaching
* Character movement depends on the character skill, status, the terrain, the weather
* Characters and Sherpas action are subject to events (injury, sickness, pneumonia, confusion, equipment loss/breakage)
* Members can only assault or retreat, can only switch once in the game, based on conditions
* Guides and sherpas can be guiding or rescueing. When rescuing, they must do triage for every distressed member they pass
* Characters can bind to one other character, but only if they have minimum energy and in the same state (assault, retreat) 
* Characters helping a distressed character are bound to that character until that character becomes unconscious or undistressed, or control character becomes distressed
* Characters can interact with each other, subject to status, terrain, weather
* Characters can rest, but might fall asleep
* Characters can rest in camp, but need minimum energy to leave camp
* Players compete with each other for sumitting, but cooperate with each other for survival
* Initial balancing can make it easy or hard to finish the game without casualties
* Game starts with bad weather, players equipping items
* Characters can start anytime with assaulting the summit

Winning strategies:
* Try to summit only one more character than opponent, make others abandon, cooperate tit-for-tat.
* Try to keep group together
* Try to make fastest climbers move fast to rescue stragglers
* Block/Sabotage opponents indirectly