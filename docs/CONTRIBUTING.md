# build system commands

Except for Java 17, the build is self-contained by gradle.

```
   # will show errorProne errors
   ./gradlew compileJava

   # will show jacoco coverage gaps
   ./gradlew test

   # runs over all subprojects and displays dependencies
   ./gradlew allDeps

   # standard task, with this project runs tests, error-prone, checkstyle, pmd, spotBugs, jacoco
   ./gradlew check -PuseSpotbugs

   # format code
   ./gradlew spotlessApply

   # checks online for updated versions to dependencies
   ./gradlew dependencyUpdate
```