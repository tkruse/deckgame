package deckgame.domain.wilderness;

import com.google.common.collect.ImmutableList;
import deckgame.domain.deck.Pile;
import deckgame.domain.deck.SimplePile;
import deckgame.domain.game.*;
import deckgame.domain.game.actions.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Sample Game about wandering in the Wilderness.
 */
public class WildernessGameStateFactory {

    public static final List<Player> PLAYERS = ImmutableList.of(
            new Player(WildernessGameRealm.REALM));
    public static final Resource FOOD = ImmutableResource.builder()
            .name("food")
            .build();
    public static final Resource WOOD = ImmutableResource.builder()
            .name("wood")
            .build();
    public static final Resource GOLD = ImmutableResource.builder()
            .name("gold")
            .build();
    public static final Resource HEALTH = ImmutableResource.builder()
            .name("health")
            .build();
    public static final Resource EXPERIENCE = ImmutableResource.builder()
            .name("xp")
            .build();

    public static GameState createGameState() {
        return new GameState(createInitialDrawDeck(), createInitialResourceCounters(), createAllActions(), PLAYERS);
    }

    private static List<GameAction> createAllActions() {
        return PLAYERS.stream().flatMap(player -> Stream.of(
                new DrawCardGameAction(player),
                new PlayCardGameAction(player),
                new ResolveEventGameAction(player),
                new FinishQuestGameAction(player))
        ).collect(Collectors.toList());
    }

    private static List<ResourceCounter> createInitialResourceCounters() {

        return Arrays.asList(
            new ResourceCounter(FOOD, 20, 20),
            new ResourceCounter(WOOD, 0, 20),
            new ResourceCounter(HEALTH, 20, 20),
            new ResourceCounter(GOLD, 0, 20),
            new ResourceCounter(EXPERIENCE, 0, -1)
        );
    }

    private static Pile createInitialDrawDeck() {
        final var cards = new SimplePile("Draw");
        cards.addAll(new WildernessCardGenerator().generateDeck(12_345, 8, 4, 2));
        cards.shuffle();
        return cards;
    }

}
