package deckgame.domain.wilderness;

import com.google.common.collect.ImmutableSet;
import deckgame.domain.deck.Card;
import deckgame.domain.deck.ImmutableGoalEventCard;
import deckgame.domain.deck.ImmutableLandmarkEventCard;
import deckgame.domain.deck.ImmutableResourceEventCard;
import deckgame.domain.deck.conditions.ImmutableCardCondition;
import deckgame.domain.deck.events.Dice;
import deckgame.domain.deck.events.Fight;
import deckgame.domain.deck.events.ImmutableCardEvent;
import deckgame.domain.deck.events.ImmutableReward;
import deckgame.domain.game.ResourceAmount;
import deckgame.domain.game.ResourceCounter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Random;
import java.util.Set;

import static deckgame.domain.wilderness.WildernessGameStateFactory.*;

public class WildernessCardGenerator {
    private final Random random = new Random();

    public Set<Card> generateInitialCollection(final long seed) {
        return generateDeck(seed, 20, 10, 3);
    }

    public Set<Card> generateDeck(final long seed, int resources, final int landmarks, int goals) {
        final var setBuilder = ImmutableSet.<Card>builder();
        random.setSeed(seed);
        for (int i = 0; i < resources; i++) {
            setBuilder.add(generateResourceCard(random));
        }
        for (int i = 0; i < landmarks; i++) {
            setBuilder.add(generateLandmarkCard(random));
        }
        for (int i = 0; i < goals; i++) {
            setBuilder.add(generateGoalCard(random));
        }
        return setBuilder.build();
    }

    public static Card generateCard(final Random random) {
        final int baseRandom = random.nextInt(100);
        if (baseRandom < 80) {
            return generateResourceCard(random);
        } else if (baseRandom < 95) {
            return generateLandmarkCard(random);
        } else {
            return generateGoalCard(random);
        }
    }

    private static Card generateResourceCard(final Random random) {
        final var terrain = Terrain.values()[random.nextInt(Terrain.values().length)];
        return ImmutableResourceEventCard.builder()
                .title(terrain.getName())
                .realm(WildernessGameRealm.REALM)
                .story(WildernessRevengeStory.STORY)
                .value(1)
                .addEventSelection(
                        ImmutableCardEvent.builder()
                                .addCost(ResourceAmount.builder().resource(FOOD).amount(2).build())
                                .build(),
                        ImmutableCardEvent.builder()
                                .addPreconditions(ResourceAmount.builder().resource(EXPERIENCE).amount(9).build())
                                .build()
                )
                .build();
    }

    private static Card generateLandmarkCard(final Random random) {
        final var landmark = Landmark.values()[random.nextInt(Landmark.values().length)];
        return ImmutableLandmarkEventCard.builder()
                .title(landmark.getName())
                .realm(WildernessGameRealm.REALM)
                .story(WildernessRevengeStory.STORY)
                .value(2)
                .addEventSelection(
                        ImmutableCardEvent.builder()
                                .addCost(ResourceAmount.builder().resource(GOLD).amount(4).build())
                                .build(),
                        ImmutableCardEvent.builder()
                                .addFights(new Fight(new Dice(5, 3), new Dice(2, -1)))
                                .addRewardSelection(ImmutableReward.builder()
                                        .addFixedResourceReward(ResourceAmount.builder().resource(EXPERIENCE).amount(2).build())
                                        .build())
                                .build(),
                        ImmutableCardEvent.builder()
                                .addPreconditions(ResourceAmount.builder().resource(EXPERIENCE).amount(12).build())
                                .build()
                )
                .build();
    }

    private static Card generateGoalCard(final Random random) {
        return ImmutableGoalEventCard.builder()
                .title("Lair")
                .realm(WildernessGameRealm.REALM)
                .story(WildernessRevengeStory.STORY)
                .value(random.nextInt())
                .condition(ImmutableCardCondition.builder()
                        .minAdventurePathLength(5)
                        .build()
                )
                .addEventSelection(
                        ImmutableCardEvent.builder()
                                .addCost(ResourceAmount.builder().resource(WOOD).amount(5).build())
                                .addRewardSelection(
                                        ImmutableReward.builder()
                                                .addFixedResourceReward(new ResourceCounter(EXPERIENCE, 5, -1))
                                                .build()
                                )
                                .build(),
                        ImmutableCardEvent.builder()
                                .addFights(new Fight(new Dice(10, 5), new Dice(4, -1)))
                                .addRewardSelection(
                                        ImmutableReward.builder()
                                                .drawRewardMaxLevel(2)
                                                .build()
                                )
                                .build(),
                        ImmutableCardEvent.builder()
                                .addPreconditions(new ResourceCounter(EXPERIENCE, 5, -1))
                                .addRewardSelection(
                                        ImmutableReward.builder()
                                                .putRandomResourceAmountReward(GOLD, new Dice(10, 5))
                                                .build()
                                )
                                .build()
                )
                .build();
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private enum Terrain {
        PLAINS("Plains"),
        GRASSLANDS("Grassland"),
        DESERT("Desert"),
        FOREST("Forest"),
        RIVER("River"),
        BEACH("Beach"),
        HILLS("Hills"),
        MOUNTAINS("Mountains"),
        /**/;

        @Getter
        private final String name;
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private enum Landmark {
        CAVE("Cave"),
        HUT("Hut"),
        CROSSING("Crossing"),
        FORD("Ford"),
        PASS("Pass"),
        BRIDGE("Bridge"),
        /**/;

        @Getter
        private final String name;
    }
}
