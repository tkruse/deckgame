package deckgame.domain.wilderness;

import deckgame.domain.collection.Story;

/**
 * A story about seeking revenge on a group of bandits which have raided your home.
 */
public class WildernessRevengeStory implements Story {

    static final Story STORY = new WildernessRevengeStory();

    private WildernessRevengeStory() {
    }

    @Override
    public String getName() {
        return "Revenge";
    }
}
