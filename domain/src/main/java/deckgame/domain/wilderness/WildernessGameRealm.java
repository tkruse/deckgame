package deckgame.domain.wilderness;

import deckgame.domain.collection.Realm;

public class WildernessGameRealm implements Realm {

    public static final Realm REALM = new WildernessGameRealm();

    private WildernessGameRealm() {
    }

    @Override
    public String getName() {
        return "Wilderness";
    }
}
