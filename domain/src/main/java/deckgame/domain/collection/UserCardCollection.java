package deckgame.domain.collection;

import java.util.Set;

public record UserCardCollection(Set<OwnedCard> cards) {
}
