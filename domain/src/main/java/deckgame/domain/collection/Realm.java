package deckgame.domain.collection;

public interface Realm {
    String getName();
}
