package deckgame.domain.collection;

import java.util.List;

/**
 * A User possibly centrally registered having purchased or otherwise received cards.
 */
public record User(String username, String email, UserCardCollection collection, List<PlayerGame> games) {
}
