package deckgame.domain.collection;

import deckgame.domain.deck.Card;

import java.util.Set;

/**
 *
 */
public record PlayerGame(String name, Set<Card> acquiredCards, Realm mainRealm, GameSetting settings) {
}
