package deckgame.domain.collection;

public interface Story {
    String getName();
}
