package deckgame.domain.collection;

public record GameSetting(boolean allowOtherRealms) {
}
