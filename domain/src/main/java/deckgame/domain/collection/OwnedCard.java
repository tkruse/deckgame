package deckgame.domain.collection;

import deckgame.domain.deck.Card;

import java.time.LocalDate;

public record OwnedCard(Card actualCard, LocalDate acquired, int usedInGames) {

}
