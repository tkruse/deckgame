package deckgame.domain.game;

import deckgame.domain.collection.Realm;
import deckgame.domain.deck.Card;
import lombok.Getter;
import lombok.Setter;

@Getter
public class Player {

    private final Realm realm;

    private final PlayerHand hand = new PlayerHand("Hand");

    @Setter
    private Card position;

    private final PlayerHand loot = new PlayerHand("Loot");
    private final PlayerHand inventory = new PlayerHand("Inventory");

    public Player(Realm realm) {
        this.realm = realm;
    }

    // inventory
}
