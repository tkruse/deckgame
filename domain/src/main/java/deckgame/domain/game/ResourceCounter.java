package deckgame.domain.game;

import lombok.Getter;

public class ResourceCounter extends ResourceAmount {
    @Getter
    private final int limit;

    public ResourceCounter(Resource resource, int amount, int limit) {
        super(resource, amount);
        this.limit = limit;
    }
}
