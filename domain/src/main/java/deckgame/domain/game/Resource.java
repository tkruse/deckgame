package deckgame.domain.game;

import org.immutables.value.Value;

@Value.Immutable
public abstract class Resource {
    public abstract String getName();
}
