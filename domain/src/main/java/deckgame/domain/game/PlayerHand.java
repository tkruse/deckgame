package deckgame.domain.game;

import deckgame.domain.deck.SimplePile;

public class PlayerHand extends SimplePile {

    public PlayerHand(String name) {
        super(name);
    }
}
