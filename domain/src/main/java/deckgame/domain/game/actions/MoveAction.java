package deckgame.domain.game.actions;

import com.google.common.collect.ImmutableList;
import deckgame.domain.deck.Card;
import deckgame.domain.game.GameState;
import deckgame.domain.game.Player;
import lombok.Getter;

import java.util.List;

public class MoveAction extends CardSelectingGameAction {

    @Getter
    private final Player player;

    public MoveAction(final Player player) {
        super();
        this.player = player;
    }

    @Override
    public String getName() {
        return "Move";
    }

    @Override
    public void execute(final GameState state, final Card card) {
        player.setPosition(card);
    }

    @Override
    public List<Card> createSelection(final GameState state) {
        final var builder = ImmutableList.<Card>builder();
        final var adventurePathPile = state.getAdventurePathPile();
        if (player.getPosition() == null) {
            builder.add(adventurePathPile.get(0));
        } else {
            final var index = adventurePathPile.indexOf(player.getPosition());
            if (index > 0) {
                builder.add(adventurePathPile.get(index - 1));
            }
            if (index < adventurePathPile.size() - 1) {
                builder.add(adventurePathPile.get(index + 1));
            }
        }
        return builder.build();
    }

    @Override
    public boolean isViable(final GameState state) {
        return state.getAdventurePathPile().size() > 1
                || (state.getAdventurePathPile().size() == 1 && player.getPosition() == null);
    }

}
