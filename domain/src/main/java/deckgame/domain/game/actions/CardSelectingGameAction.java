package deckgame.domain.game.actions;

import deckgame.domain.deck.Card;
import deckgame.domain.game.GameState;

import java.util.List;

public abstract class CardSelectingGameAction extends GameAction {

    public abstract void execute(GameState state, Card card);

    public abstract List<Card> createSelection(GameState state);

}
