package deckgame.domain.game.actions;

import deckgame.domain.game.GameState;

public class RemoveCardGameAction extends SimpleGameAction {
    @Override
    public String getName() {
        return "Forget Path";
    }

    @Override
    public void execute(final GameState state) {

    }

    @Override
    public boolean isViable(final GameState state) {
        // path not empty, no player beyond card
        return false;
    }
}
