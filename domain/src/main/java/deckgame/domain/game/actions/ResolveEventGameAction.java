package deckgame.domain.game.actions;

import deckgame.domain.deck.EventCard;
import deckgame.domain.deck.events.CardEvent;
import deckgame.domain.deck.events.SelectedReward;
import deckgame.domain.game.GameState;
import deckgame.domain.game.Player;

import javax.annotation.Nullable;

public class ResolveEventGameAction extends MoveAction {

    public ResolveEventGameAction(final Player player) {
        super(player);
    }

    public void execute(
            final GameState state,
            final EventCard card,
            final CardEvent event,
            @Nullable final SelectedReward selectedReward
    ) {
        for (final var cost : event.getCost()) {
            state.getCounterForResource(cost.getResource()).addAmount(-cost.getAmount());
        }
        if (selectedReward != null) {
            for (final var resourceChange : selectedReward.getFixedResourceReward()) {
                state.getCounterForResource(resourceChange.getResource()).addAmount(resourceChange.getAmount());
            }
            getPlayer().getLoot().addAll(selectedReward.getLoot());
        }
        super.execute(state, card);
    }
}
