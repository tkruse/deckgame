package deckgame.domain.game.actions;

import deckgame.domain.game.GameState;

public abstract class SimpleGameAction extends GameAction {

    public abstract void execute(GameState state);

}
