package deckgame.domain.game.actions;

import deckgame.domain.deck.Card;
import deckgame.domain.game.GameState;
import deckgame.domain.game.Player;

import java.util.List;

public class PlayCardGameAction extends CardSelectingGameAction {

    private final Player player;

    public PlayCardGameAction(final Player player) {
        super();
        this.player = player;
    }

    @Override
    public String getName() {
        return "Play card";
    }

    @Override
    public void execute(final GameState state, final Card card) {
        player.getHand().remove(card);
        state.getAdventurePathPile().add(card);
    }

    @Override
    public List<Card> createSelection(final GameState state) {
        return player.getHand();
    }

    @Override
    public boolean isViable(final GameState state) {
        return !player.getHand().isEmpty() && (
                player.getPosition() == null
                || state.getAdventurePathPile().indexOf(player.getPosition()) == state.getAdventurePathPile().size() - 1
                );
    }
}
