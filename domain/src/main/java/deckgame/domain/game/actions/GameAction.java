package deckgame.domain.game.actions;

import deckgame.domain.game.GameState;

public abstract class GameAction {
    public abstract String getName();

    public abstract boolean isViable(GameState state);
}
