package deckgame.domain.game.actions;

import deckgame.domain.deck.Card;
import deckgame.domain.game.GameState;
import deckgame.domain.game.Player;

import java.util.List;

import static java.util.Collections.singletonList;

public class DrawCardGameAction extends CardSelectingGameAction {

    private final Player player;

    public DrawCardGameAction(final Player player) {
        super();
        this.player = player;
    }

    @Override
    public String getName() {
        return "Draw card";
    }

    @Override
    public void execute(final GameState state, final Card card) {
        player.getHand().add(state.getDrawPile().remove());
    }

    @Override
    public List<Card> createSelection(final GameState state) {
        return singletonList(state.getDrawPile().peek());
    }

    @Override
    public boolean isViable(final GameState state) {
        return !state.getDrawPile().isEmpty() && player.getHand().size() < state.playerHandLimit(player);
    }
}
