package deckgame.domain.game.actions;

import deckgame.domain.game.GameState;
import deckgame.domain.game.Player;

public class FinishQuestGameAction extends SimpleGameAction {

    private final Player player;

    public FinishQuestGameAction(final Player player) {
        super();
        this.player = player;
    }

    @Override
    public String getName() {
        return "Finish Quest";
    }

    @Override
    public void execute(final GameState state) {
        state.finishGame(player);
    }

    @Override
    public boolean isViable(final GameState state) {
        return state.getAdventurePathPile().size() > 0
                && state.getAdventurePathPile().indexOf(player.getPosition()) == 0;
    }
}
