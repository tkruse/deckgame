package deckgame.domain.game;

import deckgame.domain.deck.Pile;
import deckgame.domain.deck.SimplePile;
import deckgame.domain.game.actions.GameAction;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class GameState {

    private final Pile adventurePathPile;

    private final Pile drawPile; // TODO: multiple piles, open goal, locked by goal condition
    private final Pile discardPile;

    private final List<ResourceCounter> resourcesCounters;

    private final List<GameAction> gameActions;

    private final List<Player> players;


    public GameState(
            final Pile drawPile,
            final List<ResourceCounter> resourcesCounters,
            final List<GameAction> gameActions,
            final List<Player> players
    ) {
        this.adventurePathPile = new SimplePile("Path");
        this.discardPile = new SimplePile("Discard");
        this.drawPile = drawPile;
        this.resourcesCounters = resourcesCounters;
        this.gameActions = gameActions;
        this.players = new ArrayList<>(players);
    }

    public int playerHandLimit(final Player player) {
        return 3;
    }

    public void finishGame(final Player player) {
        players.remove(player);
    }

    public ResourceCounter getCounterForResource(final Resource resource) {
        return resourcesCounters.stream().filter(c -> c.getResource() == resource).findFirst()
                .orElseThrow(() -> new IllegalStateException("Resource not in Quest " + resource));
    }
}
