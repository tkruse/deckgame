package deckgame.domain.game;

import deckgame.domain.deck.events.Dice;

import java.util.Random;

public class Dicer {
    private static final Random RANDOM = new Random(System.currentTimeMillis());
    public static int rollDice(Dice dice) {
        return RANDOM.nextInt(dice.range()) + dice.offset();
    }
}
