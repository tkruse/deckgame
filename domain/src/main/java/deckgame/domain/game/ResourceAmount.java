package deckgame.domain.game;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
public class ResourceAmount {
    private final Resource resource;
    @Setter
    private int amount;

    public void addAmount(int change) {
        amount = amount + change;
    }
}
