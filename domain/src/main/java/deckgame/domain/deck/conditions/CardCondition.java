package deckgame.domain.deck.conditions;

import deckgame.domain.game.GameState;
import org.immutables.value.Value;

@Value.Immutable
public abstract class CardCondition {

    /*
     * requires the players to have accumulated certain wealths
     */
    // public abstract List<ResourceCounter> getRequiredResources();

    /*
     * Requires the path to satisfy certain requirements
     */
    // public abstract List<Terrain> getCompatibleTerrains();

    // public abstract int getCompatibleTerrainsMinimumLength();

    public abstract int getMinAdventurePathLength();

    public boolean isSatisfied(final GameState state) {
        return getMinAdventurePathLength() < 0 || state.getAdventurePathPile().size() >= getMinAdventurePathLength();
    }

    public String describe() {
        if (getMinAdventurePathLength() <= 0) {
            return "";
        }
        return String.format("S%s", getMinAdventurePathLength());
    }
}
