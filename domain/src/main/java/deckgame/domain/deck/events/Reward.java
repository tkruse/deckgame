package deckgame.domain.deck.events;

import deckgame.domain.game.Resource;
import deckgame.domain.game.ResourceAmount;
import org.immutables.value.Value;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Value.Immutable
public abstract class Reward {
    public abstract List<ResourceAmount> getFixedResourceReward();
    public abstract Map<Resource, Dice> getRandomResourceAmountReward();
    public abstract Optional<Integer> getDrawRewardMaxLevel();
}
