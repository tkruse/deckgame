package deckgame.domain.deck.events;


public record Dice(int range, int offset) {
}
