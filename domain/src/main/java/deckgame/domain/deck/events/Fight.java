package deckgame.domain.deck.events;


public record Fight(Dice opponentHealth, Dice opponentAttack) {
}
