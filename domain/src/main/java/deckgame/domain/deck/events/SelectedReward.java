package deckgame.domain.deck.events;

import deckgame.domain.deck.Card;
import deckgame.domain.game.ResourceAmount;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public abstract class SelectedReward {
    public abstract List<ResourceAmount> getFixedResourceReward();
    public abstract List<Card> getLoot();
}
