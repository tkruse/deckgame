package deckgame.domain.deck.events;

import deckgame.domain.game.GameState;
import deckgame.domain.game.ResourceAmount;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public abstract class CardEvent {

    public abstract List<ResourceAmount> getPreconditions();
    public abstract List<ResourceAmount> getCost();
    public abstract List<Fight> getFights();
    public abstract List<Reward> getRewardSelection();

    public boolean isEventViable(final GameState state) {
        for (final var precond : getPreconditions()) {
            if (state.getCounterForResource(precond.getResource()).getAmount() < precond.getAmount()) {
                return false;
            }
        }
        for (final var cost : getCost()) {
            if (state.getCounterForResource(cost.getResource()).getAmount() < cost.getAmount()) {
                return false;
            }
        }
        return true;
    }

}
