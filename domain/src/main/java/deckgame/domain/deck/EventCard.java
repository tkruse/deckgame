package deckgame.domain.deck;

import deckgame.domain.deck.events.CardEvent;

import java.util.List;

public interface EventCard extends Card {

    // cardMechanic: random selection, player selection, multi-stage

    List<CardEvent> getEventSelection();
}
