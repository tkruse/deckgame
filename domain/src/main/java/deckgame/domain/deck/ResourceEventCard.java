package deckgame.domain.deck;

import deckgame.domain.deck.events.CardEvent;
import org.immutables.value.Value;

import java.util.List;

/**
 * A resource event card represents a generic event, like travelling through some forest.
 * Such cards would be very common and typically build up most of a quest deck.
 */
@Value.Immutable
public abstract class ResourceEventCard implements EventCard {

    @Override
    public abstract List<CardEvent> getEventSelection();
}
