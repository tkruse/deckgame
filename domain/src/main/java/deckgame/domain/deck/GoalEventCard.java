package deckgame.domain.deck;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public abstract class GoalEventCard implements EventCard {
    public abstract List<Card> getLoot();
}
