package deckgame.domain.deck;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

public interface Pile extends List<Card>, Queue<Card> {

    String getName();

    @Override
    default boolean offer(final Card card) {
        return this.add(card);
    }

    @Override
    default Card remove() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return poll();
    }

    @Override
    default Card poll() {
        if (isEmpty()) {
            return null;
        }
        return this.remove(lastPosition());
    }

    @Override
    default Card element() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return peek();
    }

    @Override
    default Card peek() {
        if (isEmpty()) {
            return null;
        }
        return this.get(lastPosition());
    }

    private int lastPosition() {
        return this.size() - 1;
    }

    default void shuffle() {
        Collections.shuffle(this);
    }
}
