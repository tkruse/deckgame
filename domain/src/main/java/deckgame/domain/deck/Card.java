package deckgame.domain.deck;

import deckgame.domain.collection.Realm;
import deckgame.domain.collection.Story;
import deckgame.domain.deck.conditions.CardCondition;

import java.util.Optional;

/**
 * A Card represents a place/event or an object.
 *
 */
public interface Card {
    String getTitle();

    // long serialNumber: seed defining card attributes in RNG
    Realm getRealm(); // Setting for Story, like Middle Earth, Space, GoT, Walking Dead, WW2...
    Story getStory(); // Specific setting for multiple related quests

    // Type type: restrict usage
    // decknumber: cards bought together as a set

    /**
     * Trade value for this object or the knowledge about this place
     */
    int getValue();

    /**
     * Conditions restricting playability of the card during an adventure
     */
    Optional<CardCondition> getCondition();
}
