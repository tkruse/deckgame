package deckgame.domain.deck;

import deckgame.domain.collection.Realm;
import deckgame.domain.collection.Story;

/**
 * For a goal card, the recommended card to form adventure paths
 */
public record DeckRecommendation(int resourceCardSize, int landmarkSize, int goalCardSize, Realm realm, Story story) {
}
