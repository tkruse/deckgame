package deckgame.domain.deck;

import deckgame.domain.deck.events.CardEvent;
import org.immutables.value.Value;

import java.util.List;

/**
 * A Landmark card represents a more special event than a resource card, like a cave, a bridge, a crossing.
 * They can have significant lore, and present bigger opportunity and challenge than resource cards.
 */
@Value.Immutable
public abstract class LandmarkEventCard implements EventCard {

    @Override
    public abstract List<CardEvent> getEventSelection();
}
