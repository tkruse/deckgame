package deckgame.textui.menu;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogBuilder;
import deckgame.domain.collection.OwnedCard;
import deckgame.domain.collection.UserCardCollection;
import deckgame.domain.wilderness.WildernessCardGenerator;
import deckgame.domain.wilderness.WildernessGameStateFactory;
import deckgame.textui.components.CollectionWindow;
import deckgame.textui.components.GameStateRenderer;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.stream.Collectors;

public class ActionHandler {

    private final WindowBasedTextGUI textGUI;

    public ActionHandler(final WindowBasedTextGUI textGUI) {
        this.textGUI = textGUI;
    }

    public void handleAction(final Action action) {
        if (action == Action.TUTORIAL) {
            final var newGame = WildernessGameStateFactory.createGameState();
            new GameStateRenderer(textGUI, newGame).run();
        } else if (action == Action.NEW) {
            handleNewGame();
        } else if (action == Action.LOAD) {
            new MessageDialogBuilder()
                    .setTitle("Load")
                    .setText("Not yet implemented")
                    .build()
                    .showDialog(textGUI);
        } else if (action == Action.SETTINGS) {
            new MessageDialogBuilder()
                    .setTitle("Settings")
                    .setText("Not yet implemented")
                    .build()
                    .showDialog(textGUI);
        }
    }

    private void handleNewGame() {
        // TODO use wizard to set name, select realms, settings

        final var initialCards = new WildernessCardGenerator()
                .generateInitialCollection(12_345)
                .stream()
                .map(c -> new OwnedCard(c, LocalDate.now(ZoneId.systemDefault()), 0))
                .collect(Collectors.toSet());
//        final var player1 = new Player(WildernessGameRealm.REALM);
//        final var playerGame = new PlayerGame("wilderness1", initialCards, WildernessGameRealm.REALM, new GameSetting(false));
        final UserCardCollection collection = new UserCardCollection(initialCards);
        new CollectionWindow(textGUI, collection)
                .open();
    }


    public enum Action {
        TUTORIAL,
        NEW,
        LOAD,
        SETTINGS,
        QUIT
    }
}
