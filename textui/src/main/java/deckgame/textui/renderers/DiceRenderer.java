package deckgame.textui.renderers;

import deckgame.domain.deck.events.Dice;

public class DiceRenderer {

    public static String renderDice(final Dice dice) {
        if (dice.offset() > 0) {
            return String.format("⚂%s+%s", dice.range(), dice.offset());
        }
        if (dice.offset() < 0) {
            return String.format("⚂%s%s", dice.range(), dice.offset());
        }
        return String.format("⚂%s", dice.range());
    }
}
