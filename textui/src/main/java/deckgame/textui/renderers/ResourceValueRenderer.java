package deckgame.textui.renderers;

import deckgame.domain.game.Resource;
import deckgame.domain.game.ResourceAmount;
import deckgame.domain.game.ResourceCounter;
import deckgame.domain.wilderness.WildernessGameStateFactory;

public class ResourceValueRenderer {

    public static String renderLabel(final ResourceCounter resourceCounter) {
        return resourceCounter.getResource().getName();
    }

    public static String renderValue(final ResourceCounter resourceCounter) {
        if (resourceCounter.getLimit() >= 0) {
            return String.format("%s %s/%s", renderResourceSymbol(resourceCounter), resourceCounter.getAmount(), resourceCounter.getLimit());
        } else {
            return String.format("%s %s", renderResourceSymbol(resourceCounter), resourceCounter.getAmount());
        }
    }

    public static String renderResourceSymbol(final ResourceAmount resourceCounter) {
        return renderResourceSymbol(resourceCounter.getResource());
    }

    public static String renderResourceSymbol(final Resource resource) {
        if (WildernessGameStateFactory.WOOD.equals(resource)) {
            return "≡";
        }
        if (WildernessGameStateFactory.GOLD.equals(resource)) {
            return "⛁"; // ⛁
        }
        if (WildernessGameStateFactory.EXPERIENCE.equals(resource)) {
            return "☯";
        }
        if (WildernessGameStateFactory.HEALTH.equals(resource)) {
            return "♥";
        }
        if (WildernessGameStateFactory.FOOD.equals(resource)) {
            return "Ϙ";
        }

        throw new IllegalStateException("Unknown resource " + resource);
    }
}
