package deckgame.textui.renderers;

import com.googlecode.lanterna.gui2.*;
import deckgame.domain.deck.events.CardEvent;
import deckgame.domain.deck.events.Fight;
import deckgame.domain.deck.events.Reward;
import deckgame.domain.game.ResourceAmount;

import java.util.stream.Collectors;

import static deckgame.textui.renderers.DiceRenderer.renderDice;
import static deckgame.textui.renderers.ResourceValueRenderer.renderResourceSymbol;

public class CardEventRenderer {
    public static Component renderCardEvent(final CardEvent cardEvent) {
        final var eventPanel = new Panel();

        eventPanel.setLayoutManager(new LinearLayout(Direction.HORIZONTAL));
        eventPanel.addComponent(new Label(renderCardEventConditions(cardEvent)));
        eventPanel.addComponent(new Label(" -> "));
        eventPanel.addComponent(new Label(renderCardEventRewards(cardEvent)));
        return eventPanel;
    }

    public static String renderCardEventConditions(final CardEvent cardEvent) {
        final var stringBuilder = new StringBuilder();

        for (final var precond : cardEvent.getPreconditions()) {
            stringBuilder.append(renderPrecondition(precond));
        }
        stringBuilder
                .append(renderCosts(cardEvent))
                .append(renderFights(cardEvent));

        return stringBuilder.toString();
    }

    public static String renderFights(final CardEvent cardEvent) {
        return cardEvent.getFights().stream().map(CardEventRenderer::renderFight).collect(Collectors.joining());
    }

    public static String renderCosts(final CardEvent cardEvent) {
        return cardEvent.getCost().stream().map(CardEventRenderer::renderCost).collect(Collectors.joining("+"));
    }

    public static String renderCardEventRewards(final CardEvent cardEvent) {
        final var rewards = new StringBuilder();
        if (cardEvent.getRewardSelection().isEmpty()) {
            rewards.append("pass");
        }
        for (final var reward : cardEvent.getRewardSelection()) {
            rewards.append(renderReward(reward));
        }
        return rewards.toString();
    }

    public static String renderReward(final Reward reward) {
        final var builder = new StringBuilder();
        for (final var resourceCounter : reward.getFixedResourceReward()) {
            builder.append(renderResourceSymbol(resourceCounter))
                    .append(resourceCounter.getAmount());
        }
        reward.getDrawRewardMaxLevel().ifPresent(maxLevel ->
                builder.append('⭐').append(maxLevel));
        reward.getRandomResourceAmountReward().forEach((resource, dice) -> builder.append(renderResourceSymbol(resource)).append(renderDice(dice)));
        return builder.toString();
    }

    public static String renderFight(final Fight fight) {
        return String.format("ಠ(%s,%s)",
                renderDice(fight.opponentHealth()),
                renderDice(fight.opponentAttack())
        );
    }

    public static String renderPrecondition(final ResourceAmount precond) {
        return String.format("[%s%s]",
                renderResourceSymbol(precond),
                precond.getAmount());
    }

    public static String renderCost(final ResourceAmount precond) {
        return String.format("%s%s",
                renderResourceSymbol(precond),
                precond.getAmount());
    }

    public static String renderCardExplanation(final CardEvent event) {
        final var textBuilder = new StringBuilder(30);
        if (!event.getCost().isEmpty()) {
            textBuilder.append("Give ").append(CardEventRenderer.renderCosts(event)).append(".\n");
        }
        if (!event.getFights().isEmpty()) {
            textBuilder.append("Fight ").append(CardEventRenderer.renderFights(event)).append(".\n");
        }
        if (!event.getRewardSelection().isEmpty()) {
            textBuilder.append("Get ").append(CardEventRenderer.renderCardEventRewards(event)).append(".\n");
        }
        return textBuilder.toString();
    }
}
