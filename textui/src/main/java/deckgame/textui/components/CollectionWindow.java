package deckgame.textui.components;

import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.menu.Menu;
import com.googlecode.lanterna.gui2.menu.MenuBar;
import deckgame.domain.collection.OwnedCard;
import deckgame.domain.collection.UserCardCollection;

import java.util.Arrays;

public class CollectionWindow {
    private final WindowBasedTextGUI textGUI;
    private final UserCardCollection collection;
    private final Window window;

    private final Menu menuManage;

    public CollectionWindow(final WindowBasedTextGUI textGUI, UserCardCollection collection) {
        this.textGUI = textGUI;
        this.window = new BasicWindow();
        this.collection = collection;
        menuManage = new Menu("Manage");
        final var menubar = new MenuBar();
        menubar.add(menuManage);
        menubar.add(new ExitMenu(textGUI, window));
        window.setHints(Arrays.asList(Window.Hint.FULL_SCREEN, Window.Hint.NO_DECORATIONS));

        window.setMenuBar(menubar);
    }

    public void open() {
        refreshContents();
        menuManage.takeFocus();

        textGUI.addWindowAndWait(window);
    }

    private void refreshContents() {
        final var contentPanel = new Panel();
        contentPanel.addComponent(createCollectionPanel());
        window.setComponent(contentPanel);
    }

    private Component createCollectionPanel() {
        final var handPanel = new Panel();
        handPanel.setLayoutManager(new GridLayout(5));
        for (final var card : collection.cards().stream().map(OwnedCard::actualCard).toList()) {
            CardComponentFactory.createMediumCardComponent(card, null).addTo(handPanel);
        }
        return handPanel.withBorder(Borders.singleLine("Collection"));
    }

}
