package deckgame.textui.components;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.ActionListDialogBuilder;
import deckgame.domain.deck.Card;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class CardSelectionWindow {

    private final List<Card> selection;
    private final WindowBasedTextGUI textGUI;

    public CardSelectionWindow(final List<Card> selection, final WindowBasedTextGUI textGUI) {
        this.selection = selection;
        this.textGUI = textGUI;
    }

    public Optional<Card> selectCard() {
        final var actionListDialogBuilder = new ActionListDialogBuilder()
                .setTitle("Picker")
                .setDescription("Select a card");
        final var cardSelection = new AtomicReference<Card>();
        for (final var card: this.selection) {
            actionListDialogBuilder
                    .addAction(card.getTitle(), () -> cardSelection.set(card));
        }

        actionListDialogBuilder.build()
                .showDialog(textGUI);
        return Optional.ofNullable(cardSelection.get());
    }
}
