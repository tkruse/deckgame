package deckgame.textui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.SimpleTheme;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import deckgame.domain.deck.Pile;
import deckgame.domain.game.GameState;

public class PileComponentFactory {
    public static Component createPilePanel(final Pile pile, final GameState state) {
        final var pilePanel = new Panel();
        pilePanel.setPreferredSize(new TerminalSize(10, 2));
        pilePanel.setTheme(new SimpleTheme(TextColor.ANSI.MAGENTA, TextColor.ANSI.YELLOW_BRIGHT));
        createPileComponent(pile).addTo(pilePanel);
        return pilePanel.withBorder(Borders.singleLineReverseBevel());
    }

    private static Component createPileComponent(final Pile pile) {

        final var label = new Label(pile.getName() + ": " + pile.size());
        if (pile.isEmpty()) {
            label.setTheme(new SimpleTheme(TextColor.ANSI.RED_BRIGHT, TextColor.ANSI.BLACK));
        }
        return label;
    }
}
