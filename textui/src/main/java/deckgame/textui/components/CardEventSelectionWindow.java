package deckgame.textui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import deckgame.domain.deck.EventCard;
import deckgame.domain.deck.events.CardEvent;
import deckgame.domain.game.GameState;
import deckgame.textui.renderers.CardEventRenderer;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static deckgame.textui.components.CardComponentFactory.createFullCardComponent;

public class CardEventSelectionWindow {

    private final EventCard card;
    private final GameState state;
    private final WindowBasedTextGUI textGUI;

    public CardEventSelectionWindow(final EventCard card, final GameState state, final WindowBasedTextGUI textGUI) {
        this.card = card;
        this.state = state;
        this.textGUI = textGUI;
    }

    public Optional<CardEvent> selectEvent() {
        final var window = new BasicWindow("Select one action");
        window.setHints(List.of(Window.Hint.CENTERED));
        final var borderLayout = new BorderLayout();
        final var contentPanel = new Panel(borderLayout);
        window.setComponent(contentPanel);
        final var selectedEvent = new AtomicReference<CardEvent>();
        final var explainerLabel = new Label("");
        explainerLabel.setPreferredSize(new TerminalSize(26, 3));
        explainerLabel.addTo(contentPanel);
        final var cardPanel = createFullCardComponent(
                card,
                state,
                newValue -> {
                    selectedEvent.set(newValue);
                    window.close();
                },
                cardEvent -> explainerLabel.setText(
                        CardEventRenderer.renderCardExplanation(cardEvent)
                )
        );
        cardPanel.addTo(contentPanel);
        final var cancelButton = new Button("Cancel", () -> {
            selectedEvent.set(null);
            window.close();
        });
        cancelButton.setLayoutData(BorderLayout.Location.BOTTOM);
        cancelButton.addTo(contentPanel);
        textGUI.addWindowAndWait(window);
        return Optional.ofNullable(selectedEvent.get());
    }
}
