package deckgame.textui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.gui2.menu.Menu;
import com.googlecode.lanterna.gui2.menu.MenuBar;
import com.googlecode.lanterna.gui2.menu.MenuItem;
import com.googlecode.lanterna.gui2.table.Table;
import deckgame.domain.game.GameState;
import deckgame.domain.game.actions.CardSelectingGameAction;
import deckgame.domain.game.actions.GameAction;
import deckgame.domain.game.actions.SimpleGameAction;
import deckgame.textui.components.interaction.GameActionHandler;
import deckgame.textui.renderers.ResourceValueRenderer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class GameStateRenderer {
    private final WindowBasedTextGUI textGUI;
    private final Window window;
    private final Map<GameAction, MenuItem> actionMenuItems = new HashMap<>();
    private final GameState state;

    private final Menu menuPlay;

    private final GameActionHandler gameActionHandler;

    public GameStateRenderer(final WindowBasedTextGUI textGUI, final GameState state) {
        this.textGUI = textGUI;
        this.window = new BasicWindow();
        this.state = state;

        final var menubar = new MenuBar();
        menuPlay = new Menu("Play");
        menubar.add(menuPlay);

        menubar.add(new ExitMenu(textGUI, window));

        state.getGameActions().forEach(action -> {
            final var menuItem = new MenuItem(action.getName(), () -> {
                executeAction(action);
                refreshContents();
                menuPlay.takeFocus();
            });
            actionMenuItems.put(action, menuItem);
            menuPlay.add(menuItem);
        });

        window.setHints(Arrays.asList(Window.Hint.FULL_SCREEN, Window.Hint.NO_DECORATIONS));

        window.setMenuBar(menubar);
        gameActionHandler = new GameActionHandler(textGUI, state);
    }

    private Component createResourcePanel() {
        final var resourcesPanel = new Panel();
        final var table = new Table<>("", "");
        resourcesPanel.addComponent(table);
        table.setEnabled(false);
        for (final var counter : state.getResourcesCounters()) {
            table.getTableModel().addRow(ResourceValueRenderer.renderLabel(counter), ResourceValueRenderer.renderValue(counter));
        }
        return resourcesPanel.withBorder(Borders.singleLine("Resources"));
    }

    private void executeAction(GameAction action) {
        if (action instanceof SimpleGameAction simpleGameAction) {
            simpleGameAction.execute(state);
        } else if (action instanceof CardSelectingGameAction cardSelectingGameAction) {
            gameActionHandler.handleCardSelectingGameAction(cardSelectingGameAction);
        } else {
            throw new IllegalStateException("Cannot execute action " + action);
        }
    }

    public void run() {

        refreshContents();
        menuPlay.takeFocus();

        textGUI.addWindowAndWait(window);
    }

    private void refreshContents() {
        if (state.getPlayers().isEmpty()) {
            window.close();
            return;
        }
        final var borderLayout = new BorderLayout();
        final var contentPanel = new Panel(borderLayout);
        contentPanel.setPreferredSize(new TerminalSize(60, 40));

        final var pathPanel = createPathPanel();
        pathPanel.setLayoutData(BorderLayout.Location.CENTER);
        final var resourcesPanel = createResourcePanel();
        resourcesPanel.setLayoutData(BorderLayout.Location.LEFT);
        final var handPanel = createHandPanel();
        handPanel.setLayoutData(BorderLayout.Location.BOTTOM);
        final var pilesPanel = createPilesPanel();
        pilesPanel.setLayoutData(BorderLayout.Location.RIGHT);
        contentPanel.addComponent(resourcesPanel);
        contentPanel.addComponent(pathPanel);
        contentPanel.addComponent(handPanel);
        contentPanel.addComponent(pilesPanel);
        window.setComponent(contentPanel);

        actionMenuItems.forEach((action, menuItem) -> menuItem.setEnabled(action.isViable(state)));
    }

    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private Component createPathPanel() {
        final var pathPanel = new Panel();
        for (final var card : state.getAdventurePathPile()) {
            final var pathStepPanel = new Panel();
            pathStepPanel.setLayoutManager(new LinearLayout(Direction.HORIZONTAL));
            pathStepPanel.addTo(pathPanel);
            CardComponentFactory.createMinifiedCardComponent(card, state).addTo(pathStepPanel);
            for (final var player : state.getPlayers()) {
                if (player.getPosition() == card) {
                    final var playerLabel = new Label("♗");
                    pathStepPanel.addComponent(playerLabel);
                }
            }
        }
        return pathPanel.withBorder(Borders.singleLine("Adventure Path"));
    }

    private Component createHandPanel() {
        final var handPanel = new Panel();
        handPanel.setLayoutManager(new LinearLayout(Direction.HORIZONTAL));
        for (final var card : state.getPlayers().get(0).getHand()) {
            CardComponentFactory.createMediumCardComponent(card, state).addTo(handPanel);
        }
        return handPanel.withBorder(Borders.singleLine("Hand Cards"));
    }

    private Component createPilesPanel() {
        final var pilesPanel = new Panel();
        pilesPanel.setLayoutManager(new LinearLayout(Direction.VERTICAL));
        PileComponentFactory.createPilePanel(state.getDrawPile(), state).addTo(pilesPanel);
        PileComponentFactory.createPilePanel(state.getDiscardPile(), state).addTo(pilesPanel);
        return pilesPanel.withBorder(Borders.singleLine("Piles"));
    }

}
