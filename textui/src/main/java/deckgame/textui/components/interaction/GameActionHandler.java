package deckgame.textui.components.interaction;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogBuilder;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;
import deckgame.domain.deck.EventCard;
import deckgame.domain.deck.events.CardEvent;
import deckgame.domain.deck.events.Fight;
import deckgame.domain.game.GameState;
import deckgame.domain.game.actions.CardSelectingGameAction;
import deckgame.domain.game.actions.ResolveEventGameAction;
import deckgame.textui.components.CardEventSelectionWindow;
import deckgame.textui.components.CardSelectionWindow;
import deckgame.textui.components.RewardSelectionWindow;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static deckgame.textui.renderers.CardEventRenderer.renderCardExplanation;

public class GameActionHandler {

    private final WindowBasedTextGUI textGUI;

    private final GameState state;

    public GameActionHandler(final WindowBasedTextGUI textGUI, final GameState state) {
        this.textGUI = textGUI;
        this.state = state;
    }

    public void handleCardSelectingGameAction(final CardSelectingGameAction cardSelectingGameAction) {
        final var cardOpt = new CardSelectionWindow(cardSelectingGameAction.createSelection(state), textGUI)
                .selectCard();
        cardOpt.ifPresent(card -> {
            if (cardSelectingGameAction instanceof ResolveEventGameAction resolveEventGameAction) {
                if (card instanceof EventCard eventCard && !eventCard.getEventSelection().isEmpty()) {
                    handleResolveEventAction(resolveEventGameAction, eventCard);
                } else {
                    cardSelectingGameAction.execute(state, card);
                }
            } else {
                cardSelectingGameAction.execute(state, card);
            }
        });
    }

    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private void handleResolveEventAction(final ResolveEventGameAction resolveEventGameAction, final EventCard eventCard) {
        final var resolved = new AtomicBoolean(false);
        do {
            final var cardEvent = new CardEventSelectionWindow(eventCard, state, textGUI)
                    .selectEvent();
            if (cardEvent.isEmpty()) {
                break;
            }
            final var event = cardEvent.get();
            final var successOpt = resolveEvent(event);
            successOpt.ifPresent(success -> {
                resolved.set(true);
                if (success) {
                    final var selectedReward = new RewardSelectionWindow(event.getRewardSelection(), textGUI).selectReward();
                    resolveEventGameAction.execute(state, eventCard, event, selectedReward.orElse(null));
                }
            });
        } while (!resolved.get());
    }

    private Optional<Boolean> resolveEvent(final CardEvent event) {
        final String eventText = renderCardExplanation(event);
        final var resultButton = new MessageDialogBuilder()
                .setTitle("Execute action?")
                .setText(eventText)
                .addButton(MessageDialogButton.Continue)
                .addButton(MessageDialogButton.Cancel)
                .build()
                .showDialog(textGUI);
        if (event.getFights().isEmpty() && resultButton != MessageDialogButton.Continue) {
            return Optional.empty();
        }
        return Optional.of(resolveFights(event.getFights()));
    }


    private boolean resolveFights(final List<Fight> fights) {
        var result = true;
        // TODO: fix logic
        for (final var fight : fights) {
            final var resultOpt = resolveFight(fight);
            if (resultOpt.isPresent()) {
                result &= resultOpt.get();
            } else {
                break;
            }
        }
        return result;
    }

    private Optional<Boolean> resolveFight(final Fight fight) {
        // TODO implement fights
        final var result = new MessageDialogBuilder()
                .setTitle("Fight round")
                .setText(fight.toString())
                .addButton(MessageDialogButton.Continue)
                .addButton(MessageDialogButton.Cancel)
                .build()
                .showDialog(textGUI);
        return Optional.ofNullable(result == MessageDialogButton.Continue ? true : null);
    }
}
