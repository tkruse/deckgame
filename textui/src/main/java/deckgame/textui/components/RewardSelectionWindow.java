package deckgame.textui.components;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.ActionListDialogBuilder;
import deckgame.domain.deck.events.ImmutableSelectedReward;
import deckgame.domain.deck.events.Reward;
import deckgame.domain.deck.events.SelectedReward;
import deckgame.domain.game.ResourceAmount;
import deckgame.textui.renderers.CardEventRenderer;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static deckgame.domain.game.Dicer.rollDice;

public class RewardSelectionWindow {

    private final List<Reward> selection;
    private final WindowBasedTextGUI textGUI;

    public RewardSelectionWindow(final List<Reward> selection, final WindowBasedTextGUI textGUI) {
        this.selection = selection;
        this.textGUI = textGUI;
    }

    public Optional<SelectedReward> selectReward() {
        final Reward selectedReward;
        if (selection.isEmpty()) {
            selectedReward = null;
        } else if (selection.size() == 1) {
            selectedReward = selection.get(0);
        } else {
            final var actionListDialogBuilder = new ActionListDialogBuilder()
                    .setTitle("Picker")
                    .setDescription("Select a reward")
                    .setCanCancel(false);
            final var rewardSelection = new AtomicReference<Reward>();
            for (final var reward : this.selection) {
                actionListDialogBuilder
                        .addAction(CardEventRenderer.renderReward(reward), () -> rewardSelection.set(reward));
            }
            actionListDialogBuilder.build()
                    .showDialog(textGUI);
            selectedReward = rewardSelection.get();
        }
        return Optional.ofNullable(selectedReward).map(this::resolve);
    }

    private SelectedReward resolve(final Reward selectedReward) {
        final var rewardBuilder = ImmutableSelectedReward.builder();
        for (final var fixedResource : selectedReward.getFixedResourceReward()) {
            rewardBuilder.addFixedResourceReward(fixedResource);
        }
        selectedReward.getRandomResourceAmountReward().forEach((resource, dice)
                -> rewardBuilder.addFixedResourceReward(
                        ResourceAmount.builder().resource(resource).amount(rollDice(dice)).build()));

        return rewardBuilder.build();
    }
}
