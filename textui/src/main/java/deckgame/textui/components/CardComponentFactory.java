package deckgame.textui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.SimpleTheme;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.table.Table;
import com.googlecode.lanterna.input.KeyStroke;
import deckgame.domain.deck.Card;
import deckgame.domain.deck.EventCard;
import deckgame.domain.deck.events.CardEvent;
import deckgame.domain.game.GameState;
import deckgame.textui.renderers.CardEventRenderer;

import java.util.Optional;
import java.util.function.Consumer;

public class CardComponentFactory {

    public static final SimpleTheme EMPHASIS_THEME = new SimpleTheme(TextColor.ANSI.RED_BRIGHT, TextColor.ANSI.BLACK);

    public static Component createMinifiedCardComponent(final Card card, final GameState state) {
        final var cardPanel = new Panel();
        cardPanel.setPreferredSize(new TerminalSize(10, 2));
        createConditionComponent(card, state).ifPresent(c -> c.addTo(cardPanel));
        return cardPanel.withBorder(Borders.doubleLineBevel(card.getTitle()));
    }

    private static Optional<Label> createConditionComponent(final Card card, final GameState state) {
        return card.getCondition().map(condition ->
        {
            final var label = new Label(condition.describe());
            if (state != null && !condition.isSatisfied(state)) {
                label.setTheme(EMPHASIS_THEME);
            }
            return label;
        });
    }

    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public static Component createMediumCardComponent(final Card card, final GameState state) {
        final var cardPanel = new Panel();
        cardPanel.setPreferredSize(new TerminalSize(20, 8));
        createConditionComponent(card, state).ifPresent(c -> c.addTo(cardPanel));
        if (card instanceof EventCard eventCard) {
            for (final var event : eventCard.getEventSelection()) {
                final var component = CardEventRenderer.renderCardEvent(event);
                if (state != null && !event.isEventViable(state)) {
                    component.setTheme(EMPHASIS_THEME);
                }
                cardPanel.addComponent(component);
            }
        }
        return cardPanel.withBorder(Borders.doubleLineBevel(card.getTitle()));
    }

    public static Component createFullCardComponent(
            final Card card,
            final GameState state,
            final Consumer<CardEvent> rowSelectionAction,
            final Consumer<CardEvent> listener
    ) {
        final var cardPanel = new Panel();
        cardPanel.setPreferredSize(new TerminalSize(25, 12));
        createConditionComponent(card, state).ifPresent(c -> c.addTo(cardPanel));
        final var table = new Table<>("Condition", "Effect") {
            @Override
            public Result handleKeyStroke(KeyStroke keyStroke) {
                final var result = super.handleKeyStroke(keyStroke);
                if (card instanceof EventCard eventCard) {
                    listener.accept(eventCard.getEventSelection().get(getSelectedRow()));
                }
                return result;
            }
        };
        table.addTo(cardPanel);
        if (card instanceof EventCard eventCard) {
            for (final var event : eventCard.getEventSelection()) {
                table.getTableModel().addRow(
                        CardEventRenderer.renderCardEventConditions(event),
                        event.isEventViable(state) ? CardEventRenderer.renderCardEventRewards(event) : "not possible"
                );
            }
            table.setSelectAction(() -> {
                final var cardEvent = eventCard.getEventSelection().get(table.getSelectedRow());
                if (cardEvent.isEventViable(state)) {
                    rowSelectionAction.accept(cardEvent);
                }
            });
            table.takeFocus();
        }

        return cardPanel.withBorder(Borders.doubleLineBevel(card.getTitle()));
    }
}
