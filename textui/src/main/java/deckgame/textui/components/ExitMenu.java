package deckgame.textui.components;

import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogBuilder;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;
import com.googlecode.lanterna.gui2.menu.Menu;

public class ExitMenu extends Menu {

    private final WindowBasedTextGUI textGUI;
    private final Window window;

    public ExitMenu(final WindowBasedTextGUI textGUI, final Window window) {
        super("Exit");
        this.textGUI = textGUI;
        this.window = window;
    }

    @Override
    protected boolean onActivated() {
        if (MessageDialogButton.Yes == new MessageDialogBuilder()
                .setTitle("Exit")
                .setText("Really exit?")
                .addButton(MessageDialogButton.No)
                .addButton(MessageDialogButton.Yes)
                .build()
                .showDialog(textGUI)) {
            window.close();
        }
        return true;
    }
}
